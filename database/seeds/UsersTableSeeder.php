<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'fname' => 'Saba',
            'lname' => 'Abesadze',
            'email' => 'saba72@live.com',
            'password' => bcrypt('leavingstone'),
            'isAdmin' => true
        ]);
    }
}
