<?php

use Illuminate\Database\Seeder;

class MonthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $months = [
            [
                'code' => 'Jan',
                'name:ka' => 'იანვარი',
                'name:en' => 'January'
            ],
            [
                'code' => 'Feb',
                'name:ka' => 'თებერვალი',
                'name:en' => 'February'
            ],
            [
                'code' => 'Mar',
                'name:ka' => 'მარტი',
                'name:en' => 'March'
            ],
            [
                'code' => 'Apr',
                'name:ka' => 'აპრილი',
                'name:en' => 'April'
            ],
            [
                'code' => 'May',
                'name:ka' => 'მაისი',
                'name:en' => 'May'
            ],
            [
                'code' => 'Jun',
                'name:ka' => 'ივნისი',
                'name:en' => 'June'
            ],
            [
                'code' => 'Jul',
                'name:ka' => 'ივლისი',
                'name:en' => 'July'
            ],
            [
                'code' => 'Aug',
                'name:ka' => 'აგვისტო',
                'name:en' => 'August'
            ],
            [
                'code' => 'Sep',
                'name:ka' => 'სექტემბერი',
                'name:en' => 'September'
            ],
            [
                'code' => 'Oct',
                'name:ka' => 'ოქტომბერი',
                'name:en' => 'October'
            ],
            [
                'code' => 'Nov',
                'name:ka' => 'ნოემბერი',
                'name:en' => 'November'
            ],
            [
                'code' => 'Dec',
                'name:ka' => 'დეკემბერი',
                'name:en' => 'December'
            ],
        ];

        for ($i = 0; $i < 12; $i++) {
            $month = new \App\Month();
            $month->fill($months[$i]);
            $month->save();
        }
    }
}
