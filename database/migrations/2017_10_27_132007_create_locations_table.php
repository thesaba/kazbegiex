<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('email')->nullable();
            $table->string('mnumber')->nullable();
            $table->string('working_from')->nullable();
            $table->string('working_to')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('address')->nullable();
            $table->string('facebook_page')->nullable();
            $table->string('youtube_video')->nullable();
            $table->timestamps();

            $table->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });

        Schema::create('location_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->text('additional_services')->nullable();
            $table->string('locale')->index();

            $table->unique(['location_id', 'locale']);
            $table->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_translations');
        Schema::dropIfExists('locations');
    }
}
