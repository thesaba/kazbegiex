let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admin-app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .sass('resources/assets/sass/landing.scss', 'public/css')
    .sass('resources/assets/sass/admin-app.scss', 'public/css')
    .styles([
        'resources/assets/css/codemirror.css',
        'resources/assets/css/material.css',
        'node_modules/select2/dist/css/select2.css'
    ], 'public/css/api.css')
    .js('resources/assets/js/api.js', 'public/js')
    .version()
    // .browserSync({
    //     // port : 8000,
    //     https : 'https://kazbegiexperience.dev',
    //     proxy : 'https://kazbegiexperience.dev'
    // })
    ;
