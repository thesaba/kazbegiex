<?php

return [
    0   => 'None',
    1   => 'UnknownError',
    2   => 'UnhandledException',
    3   => 'Unauthorized',
    11  => 'Validation Error',
    404 => 'Not found exception'
];