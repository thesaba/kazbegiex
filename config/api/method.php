<?php

return [
    1 => [
        'methodName' => 'getLocations',
        'methodController' => 'LocationController',
        'messageType' => 0,
        'responseMethod' => 2,
        'sample' => [
            'data' => [
                'messageId' => 1,
                'pageId' => 1,
            ]
        ]
    ],
    2 => [
        'methodName' => 'getLocationsResult',
        'messageType' => 1
    ],
    3 => [
        'methodName' => 'getLocation',
        'methodController' => 'LocationController',
        'messageType' => 0,
        'responseMethod' => 4,
        'sample' => [
            'data' => [
                'id' => 1
            ]
        ]
    ],
    4 => [
        'methodName' => 'getLocationsResult',
        'messageType' => 1
    ],
    5 => [
        'methodName' => 'getCategories',
        'methodController' => 'CategoryController',
        'messageType' => 0,
        'responseMethod' => 6,
        'sample' => [
            'data' => [
                'messageId' => 1
            ]
        ]
    ],
    6 => [
        'methodName' => 'getCategoriesResult',
        'messageType' => 1
    ],
    7 => [
        'methodName' => 'getCategory',
        'methodController' => 'CategoryController',
        'messageType' => 0,
        'responseMethod' => 8,
        'sample' => [
            'data' => [
                'id' => 2
            ]
        ]
    ],
    8 => [
        'methodName' => 'getCategoryResult',
        'messageType' => 1
    ],
    9 => [
        'methodName' => 'getLanguages',
        'methodController' => 'LanguageController',
        'messageType' => 0,
        'responseMethod' => 10,
        'sample' => [
            'data' => [
                'messageId' => 1
            ]
        ]
    ],
    10 => [
        'methodName' => 'getLanguagesResult',
        'messageType' => 1
    ],
    11 => [
        'methodName' => 'getLanguage',
        'methodController' => 'LanguageController',
        'messageType' => 0,
        'responseMethod' => 12,
        'sample' => [
            'data' => [
                'id' => 4
            ]
        ]
    ],
    12 => [
        'methodName' => 'getLanguageResult',
        'messageType' => 1
    ],
    13 => [
        'methodName' => 'getContactInformation',
        'methodController' => 'ContactController',
        'messageType' => 0,
        'responseMethod' => 14,
        'sample' => [
            'data' => [
                'messageId' => 1
            ]
        ]
    ],
    14 => [
        'methodName' => 'getContactInformationResult',
        'messageType' => 1
    ],
    15 => [
        'methodName' => 'getStickers',
        'methodController' => 'StickerController',
        'messageType' => 0,
        'responseMethod' => 16,
        'sample' => [
            'data' => [
                'messageId' => 1
            ]
        ]
    ],
    16 => [
        'methodName' => 'getStickersResult',
        'messageType' => 1
    ],
    17 => [
        'methodName' => 'getMonths',
        'methodController' => 'MonthController',
        'messageType' => 0,
        'responseMethod' => 18,
        'sample' => [
            'data' => [
                'messageId' => 1
            ]
        ]
    ],
    18 => [
        'methodName' => 'getMonthsResult',
        'messageType' => 1
    ],

    19 => [
        'methodName' => 'getFilteredLocations',
        'methodController' => 'FilterController',
        'messageType' => 0,
        'responseMethod' => 20,
        'sample' => [
            'data' => [
                'pageId' => 1,
                'messageId' => 1,
                'category' => 1,
                'working_from' => '12:00',
                'working_to' => '*',
                'months' => [
                    1,
                    2,
                    3
                ],
                'language' => '*'
            ]
        ]
    ],

    20 => [
        'methodName' => 'getFilteredLocationsResult',
        'messageType' => 1
    ],

    21 => [
        'methodName' => 'getLocationLanguages',
        'methodController' => 'LanguageController',
        'messageType' => 0,
        'responseMethod' => 22,
        'sample' => [
            'data' => [
                'locationId' => 1
            ]
        ]
    ],

    22 => [
        'methodName' => 'getLocationLanguagesResult',
        'messageType' => 1
    ],

    23 => [
        'methodName' => 'getAbout',
        'methodController' => 'AboutController',
        'messageType' => 0,
        'responseMethod' => 24,
        'sample' => [
            'data' => [

            ]
        ]
    ],

    24 => [
        'methodName' => 'getAboutResult',
        'messageType' => 1
    ],

    25 => [
        'methodName' => 'getPartners',
        'methodController' => 'PartnerController',
        'messageType' => 0,
        'responseMethod' => 26,
        'sample' => [
            'data' => [

            ]
        ]
    ],

    26 => [
        'methodName' => 'getPartnersResult',
        'messageType' => 1
    ],

    27 => [
        'methodName' => 'getPartnerById',
        'methodController' => 'PartnerController',
        'messageType' => 0,
        'responseMethod' => 28,
        'sample' => [
            'data' => [
                'partnerId' => 1
            ]
        ]
    ],

    28 => [
        'methodName' => 'getPartnerByIdResult',
        'messageType' => 1
    ]
];