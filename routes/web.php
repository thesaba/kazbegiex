<?php

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::get('', 'HomeController@index')->name('index');
Route::get('/en', 'HomeController@index')->name('index');
Route::post('contact', 'HomeController@send')->name('contact');
Route::post('locations/json', 'LocationController@json')->name('locations.json');
Route::post('locations/location/coordinates', 'LocationController@locationCoordinates')->name('locations.coordinates');
Route::post('categories/json', 'CategoryController@json')->name('categories.json');
Route::post('languages/json', 'LanguageController@json')->name('languages.json');
Route::post('months/json', 'MonthController@json')->name('months.json');
Route::post('admin/reset/password', 'PasswordresetController@init')->name('reset.init');
Route::post('admin/reset/password/finish', 'PasswordresetController@reset')->name('reset.reset');
Route::get('admin/reset/password/token/{token}', 'PasswordresetController@showResetForm')->name('reset.form');
Route::get('api/test', 'ApiTest\ApiTestController@index')->name('api.index');
Route::get('api/test/{id}', 'ApiTest\ApiTestController@sample')->name('api.sample');

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]], function() {
    Route::get('map', 'HomeController@map')->name('map');
    Route::get('contact', 'HomeController@contact')->name('contact');
    Route::get('js/lang.js', 'LangController@init')->name('assets.lang');

    Route::group(['middleware' => 'published'], function () {
        Route::get('location/{id}', 'LocationController@get')->name('location.get');
    });
});

Route::group(['as' => 'admin.', 'namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::get('', 'AdminController@index')->name('index');
    Route::post('login', 'AdminController@login')->name('login');
    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('profile', 'AdminController@profile')->name('profile');
    Route::post('profile', 'AdminController@updateProfile')->name('profile');
    Route::post('password', 'AdminController@changePassword')->name('password');
    Route::get('categories', 'CategoryController@index')->name('categories.index');
    Route::post('categories', 'CategoryController@create')->name('categories.create');
    Route::post('categories/json', 'CategoryController@json')->name('categories.json');
    Route::get('languages', 'LanguageController@index')->name('languages.index');
    Route::post('languages', 'LanguageController@create')->name('languages.create');
    Route::post('languages/json', 'LanguageController@json')->name('languages.json');
    Route::get('images/junk', 'ImageController@deleteJunk')->name('images.junk.delete');
    Route::get('locations', 'LocationController@index')->name('locations.index');
    Route::get('locations/create', 'LocationController@createForm')->name('locations.create.form');
    Route::get('locations/edit/{id}', 'LocationController@editForm')->name('locations.edit.form');
    Route::post('locations/edit/{id}', 'LocationController@update')->name('locations.update');
    Route::get('locations/delete/{id}', 'LocationController@delete')->name('locations.delete');
    Route::post('location/image/thumbnail', 'LocationController@changeThumbnail')->name('location.thumbnail');
    Route::post('location/image/thumbnail/crop', 'LocationController@cropThumbnail')->name('location.thumbnail.crop');
    Route::post('locations', 'LocationController@create')->name('locations.create');
    Route::post('images/upload', 'LocationController@upload')->name('locations.upload');
    Route::post('locations/json', 'LocationController@json')->name('locations.json');
    Route::get('contact', 'ContactController@index')->name('contact.index');
    Route::post('contact', 'ContactController@update')->name('contact.update');
    Route::get('contact/destroy', 'ContactController@destroy')->name('contact.destroy');
    Route::get('settings', 'SettingsController@index')->name('settings.index');
    Route::post('settings', 'SettingsController@update')->name('settings.update');
    Route::get('logout', 'AdminController@logout')->name('logout');
});