<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/api.css') }}" />

    <title>API</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 500;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        label{
            font-size:17px;
        }

        .select2-results__option[aria-selected] {
            cursor: pointer;
            color: #263238;
            font-weight: bold;
        }

        .container {
            width: 100%;
        }

        .row {
            width: 100%;
            display: flex;
            justify-content: space-between;
        }

        .col-md-6 {
            width: 45%;
            padding: 0 2.5%;
        }

        .selector {
            display: flex;
            justify-content: center;
        }

        .submit-request {
            display: flex;
            justify-content: center;
        }

        .CodeMirror {
            height: 50vh;
        }

        .select2 {
            width: 250px !important;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="container">
        <div class="row selector">
            <div class="col-md-12">
                <label for="service">Select Service: </label>
                <select name="service">
                    <option value="">--Select Service--</option>

                    @foreach(array_where(config('api.method'), function ($value) {
                        return $value['messageType'] == 0;
                    }) as $methodNumber => $methodData)

                        <option value="{{ $methodNumber }}">{{ $methodData['methodName'] }}({{ $methodNumber }})</option>

                    @endforeach

                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h2>Request</h2>
                <pre id="request"></pre>
            </div>
            <div class="col-md-6">
                <h2>Response</h2>
                <pre id="response"></pre>
            </div>
        </div>

        <hr>
        <div class="col-md-12 submit-request">
            <button name="submit" class="btn btn-success btn-submit-request" style="font-weight: bold;">Submit</button>
        </div>
    </div>

</div>

<script src="{{ mix('js/api.js') }}"></script>
<script src="{{ secure_asset('assets/js/codemirror.js') }}"></script>
<script src="{{ secure_asset('assets/js/formatting.js') }}"></script>

<script>
    $('select[name="service"]').select2();

    function objectHas(obj, key) {
        return key.split(".").every(function(x) {
            if(typeof obj != "object" || obj === null || ! x in obj)
                return false;
            obj = obj[x];
            return true;
        });
    }

    var requestMirror = CodeMirror(document.getElementById('request'), {
        value: "// API URL: /api/v1/request",
        mode: "application/ld+json",
        lineNumbers: true,
        lineWrapping: true,
        theme: "material"
    });

    var responseMirror = CodeMirror(document.getElementById("response"), {
        lineNumbers: false,
        matchBrackets: true,
        autoCloseBrackets: true,
        mode: "application/ld+json",
        lineWrapping: true,
        theme: "material"
    });

    function getBase64(file, input, cb) {

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            window.imageResult = reader.result;
            cb(input);
        };
        reader.onerror = function (error) {
            alert("error");
        };

    }

    Object.byString = function(o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    };



    $(document).on('change', '#file-inputs input', function(){
        var file = $(this)[0].files[0];
        getBase64(file, $(this), callback);
        function callback($input){
            $input.data('base64', window.imageResult);
        }
    });

    $("select[name='service']").change(function(){
        if($.isNumeric($(this).val())){
            requestMirror.setValue('loading...');
            $("#file-inputs").html('');
            $.ajax({
                url: '{{ url('api/test') }}/' + $(this).val(),
                success: function(data){
                    var data = data;
                    //@todo append extra data to html
                    if(objectHas(data, 'extra.files')){
                        _.each(data['extra']['files'], function(val, key){
                            $("#file-inputs").append('<label>' + key + ': <input name="'+key+'" data-path="'+val+'" data-base64="" type="file" class="upload-file-input"></label>');
                        });
                    }

                    delete data['extra'];

                    requestMirror.setValue(JSON.stringify(data, null, 4));
                },
                dataType: 'json'
            });
        }else{
            requestMirror.setValue('something\'s went wrong');
        }
    });

    $("button[name='submit']").click(function(){

        var data = JSON.parse(requestMirror.getValue());

        if($('#file-inputs input').length > 0){
            $.each($('#file-inputs input'), function(){
                var dataPath = $(this).data('path');
                var dataValue = $(this).data('base64');
                dataPathLeft = dataPath.substring(0, dataPath.lastIndexOf('['));
                dataPathRight = dataPath.substring(dataPath.lastIndexOf('[')+1, dataPath.length-1);
                console.log(dataPathLeft+' : '+dataPathRight)
                Object.byString(data.params, dataPathLeft)[dataPathRight] = dataValue;
            });
        }
        console.log(data);



        $.ajax({
            url: '{{ url('api/v1/request') }}',
            method: 'POST',
            data: data,
            success: function(data){
                responseMirror.setValue(JSON.stringify(data, null, 4));
            },
            error: function(data){
                responseMirror.setValue(JSON.stringify(data.responseJSON, null, 4));
            },
            dataType: 'json'
        });
    });

</script>

</body>
</html>
