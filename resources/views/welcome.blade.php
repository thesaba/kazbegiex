<!DOCTYPE html>
<html>
    <head>
        <title>Kazbegi Experience - Coming Soon</title>

        <link rel="stylesheet" href="{{ mix('css/landing.css') }}" />

        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ secure_asset('assets/favicons/apple-touch-icon-57x57.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ secure_asset('assets/favicons/apple-touch-icon-114x114.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ secure_asset('assets/favicons/apple-touch-icon-72x72.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ secure_asset('assets/favicons/apple-touch-icon-144x144.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ secure_asset('assets/favicons/apple-touch-icon-60x60.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ secure_asset('assets/favicons/apple-touch-icon-120x120.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ secure_asset('assets/favicons/apple-touch-icon-76x76.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ secure_asset('assets/favicons/apple-touch-icon-152x152.png') }}" />
        <link rel="icon" type="image/png" href="{{ secure_asset('assets/favicons/favicon-196x196.png') }}" sizes="196x196" />
        <link rel="icon" type="image/png" href="{{ secure_asset('assets/favicons/favicon-96x96.png') }}" sizes="96x96" />
        <link rel="icon" type="image/png" href="{{ secure_asset('assets/favicons/favicon-32x32.png') }}" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ secure_asset('assets/favicons/favicon-16x16.png') }}" sizes="16x16" />
        <link rel="icon" type="image/png" href="{{ secure_asset('assets/favicons/favicon-128.png') }}" sizes="128x128" />

        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

        <meta property="og:url" content="https://www.kazbegiexperience.com" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Kazbegi Experience" />
        <meta property="og:description" content="{{ trans('landing.desc') }}" />
        <meta property="og:image" content="{{ secure_asset('assets/img/background.png') }}" />


        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <nav class="navbar">
                <div class="logo">
                    <div class="logo-container"></div>
                </div>

                <div class="social">
                    <a href="https://www.facebook.com/LAGKazbegi/" target="_blank" class="social-item"><img src="{{ secure_asset('assets/img/facebook.svg') }}" /></a>
{{--                    <a href="#" class="social-item"><img src="{{ secure_asset('assets/img/twitter.svg') }}" /></a>--}}
{{--                    <a href="#" class="social-item"><img src="{{ secure_asset('assets/img/instagram.svg') }}" /></a>--}}

                    <div class="language">
                        <span class="selected-language">ინგ</span>
                    </div>
                </div>
            </nav>

            <div class="content">
                <div class="heading">
                    <h1 class="heading-text heading-bold">Kazbegi <span class="heading-text heading-regular">Experience</span></h1>
                </div>

                <div class="description">
                    <span class="description-content">
                        {{ trans('landing.desc') }}
                    </span>
                </div>
            </div>

            <div class="footer">
                <div class="footer-content"></div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>

        <script>
            $(document).ready(function () {
                var link = location.href;
                var segment = '';

                link = link.split('/');
                segment = link[link.length - 1];

                if (segment === 'en') {
                    $('.selected-language').text('ქარ');
                } else {
                    $('.selected-language').text('Eng');
                }

                $('.selected-language').click(function () {
                    if (segment === 'en') {
                        location.href = location.href.replace('/en', '');
                    } else {
                        location.href = location.href + 'en';
                    }
                });
            });
        </script>
    </body>
</html>