@component('mail::message')
# გამარჯობა

მომხმარებლის ელ-ფოსტა: {{ $email }}

შეტყობინება : {{ $message }}

გმადლობთ,<br>
{{ config('app.name') }}
@endcomponent
