@component('mail::message')
# გამარჯობა

თქვენ გამოგზავნეთ მოთხოვნა პაროლის აღდგენასთან დაკავშირებით

გთხოვთ ეწვიოთ ბმულს ან დააკლიკოთ ღილაკზე "პაროლის აღდგენა"

{{ route('reset.form', $token) }}

@component('mail::button', ['url' => route('reset.form', $token)])
პაროლის აღდგენა
@endcomponent

გმადლობთ,<br>
{{ config('app.name') }}
@endcomponent
