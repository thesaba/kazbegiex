<!doctype html>
<html>
    <head>
        <title>Contact</title>
    </head>
    <body>
        <form action="{{ route('contact') }}" method="POST">
            <input type="email" name="email" placeholder="email" required>
            <textarea placeholder="Message" name="message" required></textarea>

            {{ csrf_field() }}

            <input type="submit" name="submit" value="send" />
        </form>
    </body>
</html>