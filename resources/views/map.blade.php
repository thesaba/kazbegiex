@extends('layouts.master')

@section('EOH')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsy0OXtMRwUcs8ig52A5BxaWPxjcWZouk"></script>
@endsection

@section('content')
    <div class="main-section">
        <div class="columns">
            <div class="column is-half left-section">
                <div id="app">
                    <FilterComponent></FilterComponent>
                </div>
            </div>
            <div class="column is-half map-full-height">
                <div id="map-app">
                    <MapComponent></MapComponent>
                </div>
            </div>
        </div>
    </div>
@endsection