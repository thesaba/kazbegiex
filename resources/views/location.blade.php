@extends('layouts.master')

@section('EOH')
    <style>
        html {
            overflow: unset !important;
        }

        .wrapper {
            height: unset !important;
            max-height: unset !important;
        }
    </style>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsy0OXtMRwUcs8ig52A5BxaWPxjcWZouk"></script>
@endsection

@section('content')
    <div id="location-full-app" class="main-section location-main-section">
        <FullLocationComponent :location="{{ $location->toJson() }}" :locale="{{ $locale }}"></FullLocationComponent>
    </div>

    <div id="map-app-bottom">
        <BottomMapComponent :location="{{ $location->toJson() }}"></BottomMapComponent>
    </div>

    @include('parts.footer')
@endsection