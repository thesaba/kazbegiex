<!DOCTYPE html>
<html>
    <head>
        <title>Map - Kazbegi Experience</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}" />

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        @yield('EOH')
    </head>
    <body>
        <div class="loader-container">
            <div class="vue-loading-box">
                <div data-v-e0f7d754="" class="vue-loading-container">
                    <div data-v-9900d5bc="" data-v-e0f7d754="" class="sk-double-bounce">
                        <div data-v-9900d5bc="" class="sk-child sk-double-bounce1"></div>
                        <div data-v-9900d5bc="" class="sk-child sk-double-bounce2"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrapper">
            @include('parts.header')
            @yield('content')
        </div>

        <script src="{{ route('assets.lang') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>

        <script src="{{ mix('js/app.js') }}"></script>

        @yield('EOB')

        <script>
            let executed = false;
            
            let loaded = function () {
                if (!executed) {
                    $('.loader-container').fadeOut('slow');
                    
                    setTimeout(function () {
                        $('body').addClass('loaded');
                    }, 10);

                    executed = true;
                }   
            };

            $(window).on('load', loaded);

            setTimeout(loaded, 2000)
        </script>

        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-111366546-1', 'auto');
            ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->

        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-111366546-1');
        </script>
    </body>
</html>