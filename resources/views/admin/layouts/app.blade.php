<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			Admin - KazbegiExperience
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
		</script>
		<link href="{{ secure_asset('admin-assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ secure_asset('admin-assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ secure_asset('css/admin-app.css') }}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{ secure_asset('admin-assets/demo/default/media/img/logo/favicon.ico') }}" />

		@yield('EOH')
	</head>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			@include('admin.parts.header')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				@include('admin.parts.sidebar')
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">
									Dashboard
								</h3>
							</div>
						</div>
					</div>

					<div class="m-content">
						@yield('content')
					</div>
				</div>
			</div>
		</div>

		<div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">
			<i class="la la-arrow-up"></i>
		</div>

		<script src="{{ secure_asset('admin-assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ secure_asset('admin-assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ secure_asset('admin-assets/app/js/dashboard.js') }}" type="text/javascript"></script>
		<script src="{{ secure_asset('js/admin-app.js') }}" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.13/jquery.mask.min.js"></script>

		@yield('EOB')

		@if(session()->has('info'))
			<script>notify("{{ session()->get('info') }}")</script>
		@endif
	</body>
</html>
