@extends('admin.layouts.app')

@section('content')
    <div class="block-header">
        <h2>Settings</h2>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-8">
                <form action="{{ route('admin.settings.update') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                    <div class="card-body card-padding">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="logo">Change website logo</label>
                                <input type="file" name="logo" id="logo" class="form-control m-input" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="name" class="form-control m-input" placeholder="Website name" value="{{ $settings->site_name }}" />
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-4" id="table-categories">
                <div class="settings-logo-container" style="background: url({{ secure_asset('uploads/' . $settings->logo_url) }}) no-repeat; -webkit-background-size: contain;background-size: contain; width: 50%; height: 100%;"></div>
            </div>
        </div>
    </div>
@endsection