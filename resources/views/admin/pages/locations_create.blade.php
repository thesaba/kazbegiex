@extends('admin.layouts.app')

@section('EOH')
    <link rel="stylesheet" href="{{ secure_asset('admin-assets/vendors/custom/Trumbowyg/dist/ui/trumbowyg.css') }}" />
@endsection


@section('content')
    <div class="block-header" style="display: flex; justify-content: space-between; margin-bottom: 3rem;">
        <h2>Locations</h2>
        <a class="btn btn-primary" href="{{ route('admin.locations.index') }}">Back</a>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-7">
                <form action="{{ route('admin.locations.create') }}" method="POST" class="form-horizontal" role="form" id="location_form">
                    <div class="card-body card-padding" style="border-right: 1px solid rgba(44, 47, 63, 0.14); border-bottom: 1px solid rgba(44, 47, 63, 0.14);">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select name="category" id="category_select" class="form-control m-input">
                                    <option>Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="name_ka" class="form-control m-input" id="location_name" placeholder="Location Name - KA *" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="name_en" class="form-control m-input" id="location_name" placeholder="Location Name - EN" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="description_ka" id="location_description" rows="5" class="form-control m-input m-input--air" placeholder="Location Description - KA"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="description_en" id="location_description_en" rows="5" class="form-control m-input m-input--air" placeholder="Location Description - EN"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="m-dropzone dropzone m-dropzone--success dz-clickable" action="{{ route('admin.locations.upload') }}" id="m-dropzone-three">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop images here or click to upload.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Only image files are allowed for upload
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="email" name="email" class="form-control m-input" id="location_email" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="mnumber" class="form-control m-input" id="location_mnumber" placeholder="Mobile Number" maxlength="13">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="working_from" class="form-control m-input" id="location_working_from" placeholder="Working From">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="working_to" class="form-control m-input" id="location_working_to" placeholder="Working To">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select class="form-control m-bootstrap-select m_selectpicker" name="months[]" multiple>
                                        @foreach($months as $month)
                                            <option value="{{ $month->id }}" selected>{{ $month->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="latitude" class="form-control m-input" id="location_latitude" placeholder="Latitude">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="longitude" class="form-control m-input" id="location_longitude" placeholder="Longitude">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="address_ka" class="form-control m-input" id="location_longitude" placeholder="Address - KA">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="address_en" class="form-control m-input" id="location_longitude" placeholder="Address - EN">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    Select location languages
                                </label>
                                <div class="cb-container" style="width: 100%; display: flex; justify-content: space-evenly;">
                                    @foreach($languages as $language)
                                        <label class="m-checkbox m-checkbox--primary">
                                            <input type="checkbox" name="languages[]" value="{{ $language->id }}"> {{ $language->name }}
                                            <span></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    Select location sticker
                                </label>
                                <div class="cb-container" style="width: 100%; display: flex; justify-content: space-evenly;">
                                    @foreach($stickers as $sticker)
                                        <label class="m-checkbox m-checkbox--primary">
                                            <input type="radio" name="sticker" value="{{ $sticker->id }}" required /> {{ $sticker->content }}
                                            <span></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="additional_services_ka" id="location_additional_services" rows="5" class="form-control m-input m-input--air" placeholder="Additional Services - KA"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="additional_services_en" id="location_additional_services_en" rows="5" class="form-control m-input m-input--air" placeholder="Additional Services - EN"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="facebook_page" class="form-control m-input" id="location_facebook_page" placeholder="Facebook Page">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="youtube_video" class="form-control m-input" id="location_youtube_video" placeholder="Youtube Video">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input data-switch="true" type="checkbox" checked="checked" data-on-text="Published" data-handle-width="90" data-off-text="Unpublished" data-on-color="brand" name="is_published">
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-5">
                <div id="map" style="width: 100%; height: 25%"></div>
            </div>
        </div>
    </div>
@endsection

@section('EOB')
    <script>
        var mapSelector = document.getElementById('map');
        var marker;
        var map;
        var latlng = {  };

        function initMap() {
            var kazbegi_national_park = { lat: 42.7380264, lng: 44.6271956 };

            $('#location_latitude').val(kazbegi_national_park.lat);
            $('#location_longitude').val(kazbegi_national_park.lng);

            map = new google.maps.Map(mapSelector, {
                zoom: 15,
                center: kazbegi_national_park
            });

            marker = new google.maps.Marker({
                position: kazbegi_national_park,
                map: map
            });

            map.addListener('click', function (event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();

                latlng = { lat: lat, lng: lng };

                marker.setMap(null);

                placeMarker();
            });
        }

        function placeMarker() {
            marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            map.panTo(latlng);

            $('#location_latitude').val(latlng.lat);
            $('#location_longitude').val(latlng.lng);
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsy0OXtMRwUcs8ig52A5BxaWPxjcWZouk&callback=initMap"></script>

    <script>
        var DropzoneDemo = function () {
            var demos = function () {
                Dropzone.options.mDropzoneThree = {
                    paramName: "file",
                    maxFilesize: 10,
                    acceptedFiles: "image/*",
                    accept: function(file, done) {
                        done();
                    },
                    success: function(file, response) {
                        var file = response['file'];

                        $('<input>').attr({
                            type: 'hidden',
                            name: 'image[]',
                            value: 'uploads/' + file
                        }).appendTo('#location_form');
                    }
                };
            }

            return {
                init: function() {
                    demos();
                }
            };
        }();

        var BootstrapSwitch = function () {
            //== Private functions
            var demos = function () {
                // minimum setup
                $('[data-switch=true]').bootstrapSwitch();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        $(document).ready(function () {
            $('.m_selectpicker').selectpicker();
            $('#location_mnumber').mask('000 00 00 00');
        });

        DropzoneDemo.init();
        BootstrapSwitch.init();
    </script>

    <script src="{{ secure_asset('admin-assets/vendors/custom/Trumbowyg/dist/trumbowyg.js') }}"></script>

    <script>
        $('#location_description').trumbowyg();
        $('#location_description_en').trumbowyg();
        $('#location_additional_services').trumbowyg();
        $('#location_additional_services_en').trumbowyg();
    </script>
@endsection

