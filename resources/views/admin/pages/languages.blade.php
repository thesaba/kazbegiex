@extends('admin.layouts.app')

@section('content')
    <div class="block-header">
        <h2>Languages</h2>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-4">
                <form action="{{ route('admin.languages.create') }}" method="POST" class="form-horizontal" role="form">
                    <div class="card-body card-padding" style="border-right: 1px solid rgba(44, 47, 63, 0.14); border-bottom: 1px solid rgba(44, 47, 63, 0.14);">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="code" class="form-control m-input" id="lang_code" placeholder="Language Code *" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="ka[name]" class="form-control m-input" id="lang_name" placeholder="Language Name - KA" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="en[name]" class="form-control m-input" id="lang_name" placeholder="Language Name - EN">
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-8" id="table-categories"></div>
        </div>
    </div>
@endsection

@section('EOB')
    <script>
        $(document).ready(function () {
            window.currentDataTablesURL = '{!! route('admin.languages.json') !!}';

            $('#table-categories').mDatatable({
                data: {
                    type: 'remote',
                    source:  {
                        read: {
                            url: window.currentDataTablesURL
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: true,
                        webstorage: true
                    },
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false
                },
                layout: {
                    theme: 'default',
                    class: 'm-datatable--brand',
                    scroll: false,
                    height: null,
                    footer: false,
                    header: true,
                    smoothScroll: {
                        scrollbarShown: true
                    },
                    spinner: {
                        overlayColor: '#000000',
                        opacity: 0,
                        type: 'loader',
                        state: 'brand',
                        message: true
                    },
                    icons: {
                        sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
                        pagination: {
                            next: 'la la-angle-right',
                            prev: 'la la-angle-left',
                            first: 'la la-angle-double-left',
                            last: 'la la-angle-double-right',
                            more: 'la la-ellipsis-h'
                        },
                        rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
                    }
                },
                sortable: false,
                pagination: true,
                columns: [{
                    field: "id",
                    title: "#",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 40,
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                }, {
                    field: "name",
                    title: "Name",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 150
                }],
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            type: 'default',
                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },
                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },
                        info: true
                    }
                }
            });
        });
    </script>
@endsection