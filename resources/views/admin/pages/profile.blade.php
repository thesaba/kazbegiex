@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Your Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <div class="m-card-profile__pic-wrapper">
                                <img src="{{ secure_asset('admin-assets/app/media/img/users/user4.jpg')  }}" alt="">
                            </div>
                        </div>
                        <div class="m-card-profile__details">
                        <span class="m-card-profile__name">
                            {{ auth()->user()->getFullName() }}
                        </span>
                            <a href="" class="m-card-profile__email m-link">
                                {{ auth()->user()->email }}
                            </a>
                        </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                        <span class="m-nav__section-text">
                            Section
                        </span>
                        </li>
                        <li class="m-nav__item">
                            <a href="{{ route('admin.profile') }}" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                <span class="m-nav__link-title">
                                <span class="m-nav__link-wrap">
                                    <span class="m-nav__link-text">
                                        My Profile
                                    </span>
                                </span>
                            </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab" aria-expanded="true">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1" aria-expanded="true">
                        <div class="m-portlet__body">
                            <form action="{{ route('admin.profile') }}" method="POST" class="m-form m-form--fit m-form--label-align-right">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        First Name
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ auth()->user()->fname }}" name="fname">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Last Name
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ auth()->user()->lname }}" name="lname">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Email
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ auth()->user()->email }}" disabled>
                                    </div>
                                </div>

                                {{ csrf_field() }}

                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Save changes
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>

                            <form action="{{ route('admin.password') }}" method="POST" class="m-form m-form--fit m-form--label-align-right">
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Current Password
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" name="current_password" type="password">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        New Password
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" name="new_password">
                                    </div>
                                </div>

                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">
                                        Repeat Password
                                    </label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="password" name="repeat_password">
                                    </div>
                                </div>

                                {{ csrf_field() }}

                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Save changes
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                    <div class="tab-pane" id="m_user_profile_tab_2" aria-expanded="false"></div>
                </div>
            </div>
        </div>
    </div>
@endsection