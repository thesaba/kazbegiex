@extends('admin.layouts.app')

@section('EOH')
    <link rel="stylesheet" href="{{ secure_asset('admin-assets/vendors/custom/Trumbowyg/dist/ui/trumbowyg.css') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.1.3/cropper.css" />
@endsection


@section('content')
    <div class="block-header" style="display: flex; justify-content: space-between; margin-bottom: 3rem;">
        <h2>Locations</h2>
        <a class="btn btn-primary" href="{{ route('admin.locations.index') }}">Back</a>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-7">
                <form action="{{ route('admin.locations.update', $location->id) }}" method="POST" class="form-horizontal" role="form" id="location_form">
                    <div class="card-body card-padding" style="border-right: 1px solid rgba(44, 47, 63, 0.14); border-bottom: 1px solid rgba(44, 47, 63, 0.14);">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <select name="category" id="category_select" class="form-control m-input">
                                    <option value="{{ $location->category_id }}">{{ $location->category->name }}</option>
                                    @foreach($categories as $category)
                                        @if ($category->id != $location->category_id)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="name_ka" class="form-control m-input" id="location_name" placeholder="Location Name - KA *" value="{{ $location->translate('ka')->name }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="name_en" class="form-control m-input" id="location_name" placeholder="Location Name - EN" value="{{ $location->translate('en')->name }}" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="description_ka" id="location_description" rows="5" class="form-control m-input m-input--air" placeholder="Location Description - KA">{!! $location->translate('ka')->description !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="description_en" id="location_description_en" rows="5" class="form-control m-input m-input--air" placeholder="Location Description - EN">{!! $location->translate('en')->description !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="m-dropzone dropzone m-dropzone--success dz-clickable" action="{{ route('admin.locations.upload') }}" id="m-dropzone-three">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop images here or click to upload.
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Only image files are allowed for upload
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="email" name="email" class="form-control m-input" id="location_email" value="{{ $location->email }}" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="mnumber" class="form-control m-input" id="location_mnumber" value="{{ $location->mnumber }}" placeholder="Mobile Number">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="working_from" class="form-control m-input" id="location_working_from" value="{{ $location->working_from }}" placeholder="Working From">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="working_to" class="form-control m-input" id="location_working_to" value="{{ $location->working_to }}" placeholder="Working To">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <select class="form-control m-bootstrap-select m_selectpicker" name="months[]" multiple>
                                        @foreach($location->months as $month)
                                            <option value="{{ $month->id }}" selected>{{ $month->name }}</option>
                                        @endforeach

                                        @foreach($months->diff($location->months) as $m)
                                                <option value="{{ $m->id }}">{{ $m->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="latitude" class="form-control m-input" id="location_latitude" value="{{ $location->latitude }}" placeholder="Latitude">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="longitude" class="form-control m-input" id="location_longitude" value="{{ $location->longitude }}" placeholder="Longitude">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="address_ka" class="form-control m-input" id="location_longitude" value="{{ $location->translate('ka')->address }}" placeholder="Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="address_en" class="form-control m-input" id="location_longitude" value="{{ $location->translate('en')->address }}" placeholder="Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    Select location languages
                                </label>
                                <div class="cb-container" style="width: 100%; display: flex; justify-content: space-evenly;">
                                    @foreach($location->languages as $lang)
                                        <label class="m-checkbox m-checkbox--primary">
                                            <input type="checkbox" name="languages[]" value="{{ $lang->id }}" checked="true"> {{ $lang->name }}
                                            <span></span>
                                        </label>
                                    @endforeach
                                    @foreach($languages->diff($location->languages) as $l)
                                        <label class="m-checkbox m-checkbox--primary">
                                            <input type="checkbox" name="languages[]" value="{{ $l->id }}"> {{ $l->name }}
                                            <span></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    Select location sticker
                                </label>
                                <div class="cb-container" style="width: 100%; display: flex; justify-content: space-evenly;">
                                    @foreach($stickers as $sticker)
                                        <label class="m-checkbox m-checkbox--primary">
                                            @if($sticker->id == $location->sticker->id)
                                                <input type="radio" name="sticker" value="{{ $sticker->id }}" checked="true" /> {{ $sticker->content }}
                                            @else
                                                <input type="radio" name="sticker" value="{{ $sticker->id }}" /> {{ $sticker->content }}
                                            @endif
                                            <span></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="additional_services_ka" id="location_additional_services" rows="5" class="form-control m-input m-input--air" placeholder="Additional Services - KA">{!! $location->translate('ka')->additional_services !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea name="additional_services_en" id="location_additional_services_en" rows="5" class="form-control m-input m-input--air" placeholder="Additional Services l- EN">{!! $location->translate('en')->additional_services !!}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="facebook_page" class="form-control m-input" id="location_facebook_page" value="{{ $location->facebook_page }}" placeholder="Facebook Page">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="youtube_video" class="form-control m-input" id="location_youtube_video" value="{{ $location->youtube_video }}" placeholder="Youtube Video">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                @if($location->is_published)
                                    <input data-switch="true" type="checkbox" checked="checked" data-on-text="Published" data-handle-width="90" data-off-text="Unpublished" data-on-color="brand" name="is_published">
                                @else
                                    <input data-switch="true" type="checkbox" data-on-text="Published" data-handle-width="90" data-off-text="Unpublished" data-on-color="brand" name="is_published">
                                @endif
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-5">
                <div id="map" style="width: 100%; height: 25%"></div>
                <div class="images" id="location_images" style="width: 100%; height: 45%; margin-top: 5%;">
                    <div class="row" id="img-row">
                        @foreach($location->images as $image)
                            <div class="col-sm-4" style="margin-bottom: 25px;">
                                <div class="loc-img" style="background: url({{ asset($image->url) }}); -webkit-background-size: cover;background-size: cover; background-repeat: no-repeat; width: 100%; height: 140px;">
                                    @if ($image->is_thumbnail == 1)
                                        <span class="thumb is-thumb" data-img-id="{{ $image->id }}" data-loc-id="{{ $location->id }}">
                                            Thumbnail
                                        </span>
                                    @else
                                        <span class="thumb" data-img-id="{{ $image->id }}" data-loc-id="{{ $location->id }}">
                                            Mark as thumbnail
                                        </span>
                                    @endif
                                        <span class="crop" data-img-id="{{ $image->id }}" data-loc-id="{{ $location->id }}" data-target="#cropper_modal">
                                            Crop
                                        </span>
                                </div>
                            </div>
                            <img src="{{ asset($image->url) }}" data-image-id="{{ $image->id }}" name="image" style="display: none;" />
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cropper_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="width: 50vw;">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Crop Image
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="#" id="modal-cropper-container" style="height: 400px; display: block;">
                        
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" id="cropper-modal-submit">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('EOB')
    <script>
        var mapSelector = document.getElementById('map');
        var marker;
        var map;
        var latlng = {  };

        function initMap() {
            var kazbegi_national_park = { lat: {{ $location->latitude }}, lng: {{ $location->longitude }} };

            $('#location_latitude').val(kazbegi_national_park.lat);
            $('#location_longitude').val(kazbegi_national_park.lng);

            map = new google.maps.Map(mapSelector, {
                zoom: 15,
                center: kazbegi_national_park
            });

            marker = new google.maps.Marker({
                position: kazbegi_national_park,
                map: map
            });

            map.addListener('click', function (event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();

                latlng = { lat: lat, lng: lng };

                marker.setMap(null);

                placeMarker();
            });
        }

        function placeMarker() {
            marker = new google.maps.Marker({
                position: latlng,
                map: map
            });

            map.panTo(latlng);

            $('#location_latitude').val(latlng.lat);
            $('#location_longitude').val(latlng.lng);
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsy0OXtMRwUcs8ig52A5BxaWPxjcWZouk&callback=initMap"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.1.3/cropper.js"></script>

    <script>
        var DropzoneDemo = function () {
            var demos = function () {
                Dropzone.options.mDropzoneThree = {
                    paramName: "file",
                    maxFilesize: 10,
                    acceptedFiles: "image/*",
                    accept: function(file, done) {
                        done();
                    },
                    success: function(file, response) {
                        var file = response['file'];

                        $('<input>').attr({
                            type: 'hidden',
                            name: 'image[]',
                            value: 'uploads/' + file
                        }).appendTo('#location_form');
                    }
                };
            }

            return {
                init: function() {
                    demos();
                }
            };
        }();

        var BootstrapSwitch = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('[data-switch=true]').bootstrapSwitch();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
        BootstrapSwitch.init();

        $('.thumb').click(function () {
            let isthumb = $('.is-thumb');

            if (!$(this).hasClass('is-thumb')) {
                let imageID = $(this).data('img-id');
                let locationID = $(this).data('loc-id');
                let requestURL = '{{ route("admin.location.thumbnail") }}';

                isthumb.text('Mark as thumbnail');
                isthumb.removeClass('is-thumb');
                $(this).addClass('is-thumb');
                $(this).text('Thumbnail');

                $.ajax({
                    type: "POST",
                    url: requestURL,
                    data: {
                        'location': locationID,
                        'image': imageID
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        });

        $(document).ready(function () {
            var imageID;
            var locationID;
            var img;
            var image;
            var cropBoxData;
            var canvasData;
            var cropper;

            $('.crop').click(function (e) {
                imageID = $(this).data('img-id');
                locationID = $(this).data('loc-id');
                img = $(e.target).parent();
                image = $('#img-row').find(`[data-image-id='${imageID}']`)[0];

                $('#cropper_modal').modal();
            });

            $('#cropper_modal').on('shown.bs.modal', function () {
                $('#modal-cropper-container').append(image);

                cropper = new Cropper(image, {
                    dragMode: 'move',
                    aspectRatio: 16 / 9,
                    autoCropArea: 0.5,
                    restore: false,
                    guides: false,
                    center: true,
                    highlight: false,
                    modal: true,
                    background: false,

                    ready: function () {
                        cropper.setCropBoxData(cropBoxData).setCanvasData(canvasData);
                    }
                });
            }).on('hidden.bs.modal', function () {
                $('#modal-cropper-container').empty();

                cropBoxData = cropper.getCropBoxData();
                canvasData = cropper.getCanvasData();

                cropper.destroy();
            });

            $('#cropper-modal-submit').click(function () {
                cropper.getCroppedCanvas().toBlob(function (blob) {
                    var formData = new FormData();

                    formData.append('image', blob);
                    formData.append('imageID', imageID);
                    formData.append('locationID', locationID);

                    $.ajax('/admin/location/image/thumbnail/crop', {
                        method: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (res) {
                            console.log(res);
                            $('#cropper_modal').modal('hide');
                            window.location.reload();
                        },
                        error: function (err) {
                            console.log(err);
                            $('#cropper_modal').modal('hide');
                            alert('upload error');
                        }
                    });
                });
            });
        });

        $(document).ready(function () {
            $('.m_selectpicker').selectpicker();
            $('#location_mnumber').mask('000 00 00 00');
        });
    </script>

    <script src="{{ secure_asset('admin-assets/vendors/custom/Trumbowyg/dist/trumbowyg.js') }}"></script>

    <script>
        $('#location_description').trumbowyg();
        $('#location_description_en').trumbowyg();
        $('#location_additional_services').trumbowyg();
        $('#location_additional_services_en').trumbowyg();
    </script>
@endsection

