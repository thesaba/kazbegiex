@extends('admin.layouts.app')

@section('EOH')
    <link rel="stylesheet" href="{{ secure_asset('admin-assets/vendors/custom/Trumbowyg/dist/ui/trumbowyg.css') }}" />
@endsection

@section('content')
    <div class="block-header" style="display: flex; justify-content: space-between; margin-bottom: 3rem;">
        <h2>Locations</h2>
        <div style="display: flex;">
            <input type="text" name="locations_search" id="locations_search" class="form-control m-input" placeholder="Search Location" style="margin-right: 20px;" />

            <a class="btn btn-primary" href="{{ route('admin.locations.create.form') }}" style="margin-right: 20px;">Create new</a>
            <a class="btn btn-danger" href="{{ route('admin.images.junk.delete') }}">Clean junk images - {{ junkCount() }} image(s)</a>
        </div>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-12" id="table-locations"></div>
        </div>
    </div>

    <!--begin::Modal-->
    <div class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        Delete Location
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body" id="images-modal-body">
                    <span>Are you sure you want to delete location ?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="button" class="btn btn-danger" id="delete-location">
                        Delete
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->
@endsection

@section('EOB')
    <script>
        $(document).ready(function () {
            window.currentDataTablesURL = '{!! route('admin.locations.json') !!}';

            let locations_datatable = $('#table-locations').mDatatable({
                data: {
                    type: 'remote',
                    source:  {
                        read: {
                            url: window.currentDataTablesURL
                        }
                    },
                    pageSize: 10,
                    saveState: {
                        cookie: true,
                        webstorage: true
                    },
                    serverPaging: false,
                    serverFiltering: false,
                    serverSorting: false,
                },
                layout: {
                    theme: 'default',
                    class: 'm-datatable--brand',
                    scroll: false,
                    height: null,
                    footer: false,
                    header: true,
                    smoothScroll: {
                        scrollbarShown: true
                    },
                    spinner: {
                        overlayColor: '#000000',
                        opacity: 0,
                        type: 'loader',
                        state: 'brand',
                        message: true
                    },
                    icons: {
                        sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
                        pagination: {
                            next: 'la la-angle-right',
                            prev: 'la la-angle-left',
                            first: 'la la-angle-double-left',
                            last: 'la la-angle-double-right',
                            more: 'la la-ellipsis-h'
                        },
                        rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
                    }
                },
                sortable: false,
                pagination: true,
                search: {
                    input: $('#locations_search')
                },
                columns: [{
                    field: "id",
                    title: "#",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 40,
                    selector: {class: 'm-checkbox--solid m-checkbox--brand'}
                }, {
                    field: "name",
                    title: "Name",
                    locked: {left: 'xl'},
                    sortable: false,
//                    width: 150
                },{
                    field: "category",
                    title: "Category",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 80,
                    template: function (row) {
                        var id = row.category.name;

                        return id;
                    }
                }, {
                    field: "edit",
                    title: "Edit",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 50,
                    template: function (row) {
                        var id = row.id;

                        return '\
                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-location-edit" title="Edit Location" id="' + id + '">\
                                <i class="la la-edit"></i>\
                            </a>\
					    ';
                    }
                }, {
                    field: "delete",
                    title: "Delete",
                    locked: {left: 'xl'},
                    sortable: false,
                    width: 50,
                    template: function (row) {
                        var id = row.id;

                        return '\
                            <a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-location-delete" title="Delete Location" id="' + id + '">\
                                <i class="la la-trash"></i>\
                            </a>\
					    ';
                    }
                }],
                toolbar: {
                    layout: ['pagination', 'info'],
                    placement: ['bottom'],
                    items: {
                        pagination: {
                            type: 'default',
                            pages: {
                                desktop: {
                                    layout: 'default',
                                    pagesNumber: 6
                                },
                                tablet: {
                                    layout: 'default',
                                    pagesNumber: 3
                                },
                                mobile: {
                                    layout: 'compact'
                                }
                            },
                            navigation: {
                                prev: true,
                                next: true,
                                first: true,
                                last: true
                            },
                            pageSizeSelect: [10, 20, 30, 50, 100]
                        },
                        info: true
                    }
                }
            });

            $('#locations_search').on('change', function () {
                locations_datatable.search($(this).val());
            });
        });

        $(document).on('click', '.btn-location-edit', function (event) {
            event.preventDefault();

            let elementID = $(this).attr('id');

            location.href = '/admin/locations/edit/' + elementID;
        });

        $(document).on('click', '.btn-location-delete', function (event) {
            event.preventDefault();

            let elementID = $(this).attr('id');

            $('#delete_modal').modal();
            
            $('#delete-location').click(function () {
                location.href = '/admin/locations/delete/' + elementID;

                $('#delete_modal').modal('hide');
            });
        });
    </script>
@endsection