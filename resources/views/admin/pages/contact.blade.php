@extends('admin.layouts.app')

@section('content')
    <div class="block-header">
        <h2>Contact</h2>
    </div>

    <div class="card">
        <div class="row">
            <div class="col-sm-4">
                <form action="{{ route('admin.contact.update') }}" method="POST" class="form-horizontal" role="form">
                    <div class="card-body card-padding">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="address" class="form-control m-input" id="contact_address" placeholder="Address *" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="email" name="email" class="form-control m-input" id="contact_email" placeholder="Email *" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="mnumber" class="form-control m-input" id="contact_mnumber" placeholder="Mobile Number *" required>
                            </div>
                        </div>

                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-sm-8">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-placeholder-2"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Contact Information
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.contact.destroy') }}" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                        <i class="la la-close"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <p>
                            Address: {{ $contact->address }}
                        </p>

                        <p>
                            Email: {{ $contact->email }}
                        </p>

                        <p>
                            Mobile Number: {{ $contact->mnumber }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection