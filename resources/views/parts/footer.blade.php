<footer class="footer">
    <div class="footer-container">
        <div class="footer-logo-container">
            <div class="footer-logo" onclick="location.href = '/'"></div>
        </div>

        <div class="footer-copyright-container">
            <span class="footer-copyright">2017. All rights reserved</span>
        </div>

        <div class="footer-mailbox-container">
            <input type="email" name="email" class="footer-mailbox" placeholder="yourmail@gmail.com" autocomplete="off" >
        </div>

        <div class="footer-socials-container">
            <div class="footer-socials">
                <a href="#">
                    <img src="{{ secure_asset('assets/img/facebook-f.png') }}" />
                </a>

                <a href="#">
                    <img src="{{ secure_asset('assets/img/twitter-f.png') }}" />
                </a>

                <a href="#">
                    <img src="{{ secure_asset('assets/img/instagram-f.png') }}" />
                </a>
            </div>
        </div>
    </div>
</footer>