<header class="header">
    <div class="left-nav">
        <div class="burger-container">
            <div class="menu-button">
                <span class="menu-button-item"></span>
                <span class="menu-button-item"></span>
                <span class="menu-button-item"></span>
            </div>
            <aside>
                <ul class="burger-menu navigation-burger-menu">
                    <li class="burger-menu-item"><a href="{{ route('index') }}">@lang('navigation.home')</a></li>
                    <li class="burger-menu-item"><a href="#">@lang('navigation.state_regulations')</a></li>
                    <li class="burger-menu-item"><a class="selected" href="{{ route('map') }}">@lang('navigation.map')</a> </li>
                    <li class="burger-menu-item"><a href="#">@lang('navigation.about_us')</a> </li>
                    <li class="burger-menu-item"><a href="#">@lang('navigation.contact')</a></li>
                    <li class="burger-menu-item"><a href="#">@lang('navigation.partners')</a> </li>
                </ul>

                <ul class="burger-menu">
                    <li class="burger-menu-item burger-menu-item-socials">
                        <a href="#">
                            <img src="{{ secure_asset('assets/img/facebook-f.png') }}" />
                        </a>

                        <a href="#">
                            <img src="{{ secure_asset('assets/img/twitter-f.png') }}" />
                        </a>

                        <a href="#">
                            <img src="{{ secure_asset('assets/img/instagram-f.png') }}" />
                        </a>
                    </li>
                </ul>
            </aside>
        </div>

        <div class="logo-container">
            <div class="logo" onclick="location.href = '/'"></div>
        </div>

        <div class="navigation-container">
            <ul class="navigation">
                <li class="navigation-item"><a class="{{ request()->is('*/') ? 'selected' : '' }}" href="{{ route('index') }}">@lang('navigation.home')</a></li>
                <li class="navigation-item"><a class="{{ request()->is('*/state-regulations') ? 'selected' : '' }}" href="#">@lang('navigation.state_regulations')</a></li>
                <li class="navigation-item"><a class="{{ request()->is('*/map') ? 'selected' : '' }}" href="{{ route('map') }}">@lang('navigation.map')</a> </li>
                <li class="navigation-item"><a class="{{ request()->is('*/about') ? 'selected' : '' }}" href="#">@lang('navigation.about_us')</a> </li>
                <li class="navigation-item"><a class="{{ request()->is('*/contact') ? 'selected' : '' }}" href="#">@lang('navigation.contact')</a></li>
                <li class="navigation-item"><a class="{{ request()->is('*/partners') ? 'selected' : '' }}" href="#">@lang('navigation.partners')</a> </li>
            </ul>
        </div>
    </div>

    <div class="right-nav">
        <div class="language-container">
            <span class="navigation-item">
                @if(app()->getLocale() === 'ka')
                    <a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">Eng</a>
                @else
                    <a href="{{ LaravelLocalization::getLocalizedURL('ka', null, [], true) }}">ქარ</a>
                @endif
            </span>
        </div>
    </div>
</header>