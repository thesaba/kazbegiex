export default function(selector) {
    return _.get(window.i18n, selector);
}