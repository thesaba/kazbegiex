import Vue from 'vue';
import trans from './trans';

Vue.component('locationcomponent', require('./components/LocationComponent.vue'));
Vue.component('filtercomponent', require('./components/FilterComponent.vue'));
Vue.component('mapcomponent', require('./components/MapComponent.vue'));
Vue.component('fulllocationcomponent', require('./components/FullLocationComponent.vue'));
Vue.component('bottommapcomponent', require('./components/BottomMapComponent.vue'));

Vue.mixin({ methods: { trans } });

if (document.querySelector('#app')) {
    new Vue({
        el: '#app'
    });
}

if (document.querySelector('#map-app')) {
    window.mapApp = new Vue({
        el: '#map-app'
    });
}

if (document.querySelector('#location-full-app')) {
    new Vue({
        el: '#location-full-app'
    });
}

if (document.querySelector('#map-app-bottom')) {
    window.mapApp = new Vue({
        el: '#map-app-bottom'
    });
}