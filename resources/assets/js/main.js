$(document).ready(function () {
    $(document).on('click', '.dropdown', function (event) {
        event.preventDefault();
        var isThisActive = $(this).hasClass('is-active');
        $('.dropdown.is-active').not('.months-dropdown.is-active').removeClass('is-active');

        if (!isThisActive) {
            $(this).toggleClass('is-active');
        }
    });

    $(document).on('click', function(e) {
        if ($(e.target).closest('.dropdown').length === 0) {
            $('.dropdown').removeClass('is-active');
        }
    });

    $('.dropdown-item').not('.dropdown-category-item').click(function () {
        var selectedValue = $(this).text();

        $(this).closest('.dropdown').find('.selected-value').text(selectedValue);
    });

    $('.location-image').on('click', function (e) {
        let imageUrl = $(e.target).css('background-image').split('"')[1];

        $('.location-main-image').css({
           'background-image': 'url(' + imageUrl + ')'
        });
    });

    $('.menu-button').click(function() {
        $(this).toggleClass('active');

        if ($(this).hasClass("active")) {
            $('body').css({
                'position': 'fixed',
                'overflow': 'hidden'
            });

            $('.menu-button-item:nth-child(2)').addClass('transparent');
            $('.menu-button-item:nth-child(1)').addClass('rotate-top');
            $('.menu-button-item:nth-child(3)').addClass('rotate-bottom');

            $('.wrapper').addClass('wrapper-slide');
            $('aside').addClass('aside-slide');
        }
        else {
            $('body').css({
                'position': 'unset',
                'overflow': 'unset'
            });

            $('.menu-button-item:nth-child(2)').removeClass('transparent');
            $('.menu-button-item:nth-child(1)').removeClass('rotate-top');
            $('.menu-button-item:nth-child(3)').removeClass('rotate-bottom');

            $('.wrapper').removeClass('wrapper-slide');
            $('aside').removeClass('aside-slide');
        }
    });

    $('#working_from, #working_to').timepicker({
        timeFormat: 'H:i'
    });

    let didScroll;
    let lastScrollTop = 0;
    let delta = 5;
    let headerHeight = $('header').outerHeight();
    let documentWidth = $(document).width();

    $('.left-section').scroll(function(event) {
        if (documentWidth > 414) {
            didScroll = true;
        }
    });

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        let st = $('.left-section').scrollTop();

        if(Math.abs(lastScrollTop - st) <= delta) {
            return;
        }

        if (st > lastScrollTop && st > headerHeight) {
            $('header').removeClass('header-down').addClass('header-up');
            $('.map-full-height').removeClass('map-down').addClass('map-up');
            $('.left-section').removeClass('map-down').addClass('map-up');
        } else {
            $('header').removeClass('header-up').addClass('header-down');
            $('.map-full-height').removeClass('map-up').addClass('map-down');
            $('.left-section').removeClass('map-up').addClass('map-down');
        }

        lastScrollTop = st;
    }
});