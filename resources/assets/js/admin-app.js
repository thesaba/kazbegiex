require("timepicker");

$(document).ready(function () {
   $('#location_working_from, #location_working_to').timepicker({
       minuteStep: 1,
       showSeconds: false,
       showMeridian: false,
       snapToStep: true
   });

    $('input[type=email]').bind('keyup', function () {
        this.value = this.value.replace(/[^a-zA-Z@0-9.-_]/i, '');
    });
});