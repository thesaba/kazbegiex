<?php

return [
    'working_hours' => 'სამუშაო საათები',
    'description' => 'აღწერა',
    'additional_services' => 'დამატებითი სერვისები',
    'contact' => 'კონტაქტი',
    'languages' => 'ენები',
    'email' => 'ელ-ფოსტა',
    'tel' => 'ტელ',
    'address' => 'მისამართი',
    'show' => 'გამოჩენა',
    'youtube_video' => 'Youtube ვიდეო',
    'show_video' => 'ვიდეოს ნახვა'
];