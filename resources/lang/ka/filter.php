<?php

return [
    'category' => 'კატეგორია',
    'working_hours' => 'სამუშაო საათები',
    'from' => 'დან',
    'to' => 'მდე',
    'language' => 'ენა',
    'all' => 'ყველა',
    'months' => 'თვეები',
    'month' => 'თვე',
];