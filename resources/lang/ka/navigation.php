<?php

return [
    'home' => 'მთავარი',
    'state_regulations' => 'სახელმწიფო რეგულაციები',
    'map' => 'რუკა',
    'about_us' => 'ჩვენს შესახებ',
    'contact' => 'კონტაქტი',
    'partners' => 'პარტნიორები',
];