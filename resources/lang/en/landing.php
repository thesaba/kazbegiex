<?php

return [
    'desc' => 'Kazbegi Local Action Group developed strategy for local branding – Kazbegi Brand as quality standard of local products and services. Soon you can find detailed information on this web-page about producers and service providers that represent the brand and Kazbegi brand mobile application will provide you access to all brand members.'
];