<?php

return [
    'category' => 'Category',
    'working_hours' => 'Working Hours',
    'from' => 'From',
    'to' => 'To',
    'language' => 'Language',
    'all' => 'All',
    'months' => 'Months',
    'month' => 'Month',
];