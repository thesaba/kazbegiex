<?php

return [
    'working_hours' => 'Working Hours',
    'description' => 'Description',
    'additional_services' => 'Additional Services',
    'contact' => 'Contact',
    'languages' => 'Languages',
    'email' => 'E-mail',
    'tel' => 'Tel',
    'address' => 'Address',
    'show' => 'Show',
    'youtube_video' => 'Youtube Video',
    'show_video' => 'Show Video'
];