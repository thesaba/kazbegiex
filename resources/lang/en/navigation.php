<?php

return [
    'home' => 'Home',
    'state_regulations' => 'State Regulations',
    'map' => 'Map',
    'about_us' => 'About us',
    'contact' => 'Contact',
    'partners' => 'Partners',
];