<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutTranslation extends Model
{
    protected $table = 'about_translations';

    public $timestamps = false;

    protected $fillable = ['about'];
}
