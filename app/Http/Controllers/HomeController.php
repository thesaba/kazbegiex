<?php

namespace App\Http\Controllers;

use App\Category;
use App\Language;
use App\Location;
use App\Mail\UserContacted;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $locale = basename(request()->url());

        app()->setLocale('ka');

        if ($locale == 'en') app()->setLocale('en');

        return view('welcome');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function map()
    {
        return view('map');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        return view('contact');
    }
    
    public function send()
    {
        Mail::to('leavingstoneweb@gmail.com')
            ->queue(new UserContacted(request()->get('email'), request()->get('message')));
    }
}
