<?php

namespace App\Http\Controllers;

use App\Location;

class LocationController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        app()->setLocale(getPreviousLocale(url()->previous()));

        $category = request()->get('category');
        $working_from = request()->get('working_from');
        $working_to = request()->get('working_to');
        $language = request()->get('language');
        $months = request()->get('months');

        $locations = Location::where('is_published', true)->with('thumbnail', 'sticker', 'months')->orderBy('created_at', 'desc');

        if ($category != '*' && $category != 'All')
        {
            $locations->whereHas('category', function($query) use ($category) {
                $query->whereHas('translations', function($q) use ($category) {
                    $q->where('name', $category);
                });
            });
        }

        if ($working_from != '*')
        {
            $locations->where('working_from', 'LIKE', $working_from);
        }

        if ($working_to != '*')
        {
            $locations->where('working_to', 'LIKE', $working_to);
        }

        if (!empty($months))
        {
            $locations->whereHas('months', function ($query) use ($months) {
                $query->whereHas('translations', function ($transQuery) use ($months) {
                    sizeof($months == 12) ? $transQuery->where('name', $months) : $transQuery->whereIn('name', $months);
                });
            });
        }

        if($language != '*')
        {
            $locations->whereHas('languages', function($query) use ($language) {
                $query->whereTranslation('name', $language);
            });
        }

        return response()->json(stripLocationTags($locations->get()));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function locationCoordinates()
    {
        app()->setLocale(getPreviousLocale(url()->previous()));

        $locations = Location::join('location_translations as t', function ($join) {
            $join->on('locations.id', '=', 't.location_id');
        })->with('translations')->get(['name', 'latitude', 'longitude']);

        return response()->json($locations);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function get($id)
    {
        $location = Location::with(['category', 'thumbnail', 'images', 'languages', 'sticker'])->where('id', $id)->firstOrFail();
        $locale = json_encode([ 'locale' => app()->getLocale() ]);

        $description = $location->description;
        $additional_services = $location->additional_services;

        $location->description = strip_tags($description, '<p><a>');
        $location->additional_services = strip_tags($additional_services, '<p><a>');

        return view('location', compact('location', 'locale'));
    }
}
