<?php

namespace App\Http\Controllers;

use App\Month;

class MonthController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        app()->setLocale(getPreviousLocale(url()->previous()));

        $months = Month::all();

        return response()->json($months);
    }
}
