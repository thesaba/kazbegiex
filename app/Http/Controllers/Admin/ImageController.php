<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteJunk()
    {
        $localImagesArr = [];

        foreach (glob('uploads/images' . '/*') as $image)
        {
            $localImagesArr[] = [
                'url' => secure_asset($image)
            ];
        }

        $imagesArr = Image::all(['url'])->toArray();

        foreach ($localImagesArr as $localImage) {
            $duplicate = false;

            foreach ($imagesArr as $imageKey => $imageValue)
            {
                if ($localImage['url'] === $imageValue['url'])
                {
                    unset($imagesArr[$imageKey]);
                    $duplicate = true;
                }
            }

            if ($duplicate === false)
            {
                $imploded = implode('', $localImage);
                $exploded = explode(url('/') . '/', $imploded)[1];

                unlink($exploded);
            }
        }

        return redirect()->back()->with('info', 'Junk has been successfully deleted');
    }
}
