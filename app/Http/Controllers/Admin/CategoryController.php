<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.categories');
    }

    /**
     * @return string JSON
     */
    public function json()
    {
        return json_encode(Category::all());
    }

    /**
     * @param CategoryRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(CategoryRequest $request, Category $category)
    {
        $category->fill([
            'name:ka' => $request->get('name_ka'),
            'name:en' => $request->get('name_en')
        ]);

        $category->save();

        return redirect()->back()->with('info', 'Category has been successfully created');
    }
}
