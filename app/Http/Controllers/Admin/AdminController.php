<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin')->except(['index', 'login']);
        $this->middleware('guest')->only('login');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        if (auth()->check())
            return redirect()->route('admin.dashboard');

        return view('admin.auth.login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login()
    {
        $attributes = request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $remember = request()->has('keep_me_signed_in');

        if (!auth()->attempt($attributes, $remember)) return redirect()->back()->with('info', 'Invalid username or password');

        return redirect()->route('admin.dashboard')->with('info', 'Successfully logged in');
    }
    
    public function dashboard()
    {
        return view('admin.pages.dashboard');
    }
    
    public function profile()
    {
        return view('admin.pages.profile');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile()
    {
        list($fname, $lname) = array_values(request()->validate([
            'fname' => 'required|min:3',
            'lname' => 'required|min:3'
        ]));

        $user = auth()->user();

        $user->fname = $fname;
        $user->lname = $lname;

        $user->update();

        return redirect()->back()->with('info', 'Profile has been successfully updated');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword()
    {
        list($current_password, $password) = array_values(request()->validate([
            'current_password' => 'required',
            'new_password' => 'required',
            'repeat_password' => 'required|same:new_password'
        ]));

        $old_password = auth()->user()->password;

        if (Hash::check($current_password, $old_password))
        {
            auth()->user()->password = bcrypt($password);

            auth()->user()->update();

            return redirect()->back()->with('info', 'Password has been successfully changed');
        }

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        auth()->logout();

        return redirect()->route('admin.index');
    }
}
