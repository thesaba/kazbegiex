<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $contact = optional(Contact::first());

        return view('admin.pages.contact', compact('contact'));
    }

    /**
     * @param ContactRequest $request
     * @param Contact $contact
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ContactRequest $request, Contact $contact)
    {
        $contact->truncate();

        $contact->create($request->all());

        return redirect()->back()->with('info', 'Contact information has been successfully updated');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        Contact::truncate();

        return redirect()->back()->with('info', 'Contact information has been successfully destroyed');
    }
}
