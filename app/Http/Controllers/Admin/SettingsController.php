<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;
use App\Settings;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $settings = Settings::first();

        return view('admin.pages.settings', compact('settings'));
    }

    /**
     * @param SettingsRequest $request
     * @param Settings $settings
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SettingsRequest $request, Settings $settings)
    {
        $logo = $request->file('logo');
        $url = Storage::disk('uploads')->put('settings', $logo);
        $name = $request->get('name');

        $settings->truncate();
        $settings->create([
            'logo_url' => $url,
            'site_name' => $name
        ]);

        return redirect()->back()->with('info', 'Settings has been successfully updated');
    }
}
