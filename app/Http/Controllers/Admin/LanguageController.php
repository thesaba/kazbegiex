<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LanguageRequest;
use App\Language;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * LanguageController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.languages');
    }

    /**
     * @param LanguageRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(LanguageRequest $request)
    {
        Language::create($request->all());

        return redirect()->back()->with('info', 'Language has been successfully created');
    }

    /**
     * @return string
     */
    public function json()
    {
        return json_encode(Language::all());
    }
}
