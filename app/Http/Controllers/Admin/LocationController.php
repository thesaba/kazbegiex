<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LocationRequest;
use App\Image;
use App\Language;
use App\Location;
use App\LocationLanguage;
use App\LocationMonth;
use App\Month;
use App\Sticker;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class LocationController extends Controller
{
    /**
     * LocationController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.pages.locations');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createForm()
    {
        $categories = Category::all();
        $languages = Language::all();
        $stickers = Sticker::all();
        $months = Month::all();

        return view('admin.pages.locations_create', compact('categories', 'languages', 'stickers', 'months'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editForm($id)
    {
        $location = Location::where('id', $id)->first();
        $categories = Category::all();
        $languages = Language::all();
        $stickers = Sticker::all();
        $months = Month::all();

        return view('admin.pages.locations_edit', compact('location', 'categories', 'languages', 'stickers', 'months'));
    }

    /**
     * @param LocationRequest $request
     * @param Location $location
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(LocationRequest $request, Location $location)
    {
        $images = $request->get('image');
        $is_published = ($request->get('is_published')) ? true : false;

        $loc = $location->fill([
            'category_id' => $request->get('category'),
            'sticker_id' => $request->get('sticker'),
            'name:ka' => $request->get('name_ka'),
            'name:en' => $request->get('name_en'),
            'description:ka' => $request->get('description_ka'),
            'description:en' => $request->get('description_en'),
            'email' => $request->get('email'),
            'mnumber' => $request->get('mnumber'),
            'working_from' => $request->get('working_from'),
            'working_to' => $request->get('working_to'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'address:ka' => $request->get('address_ka'),
            'address:en' => $request->get('address_en'),
            'additional_services:ka' => $request->get('additional_services_ka'),
            'additional_services:en' => $request->get('additional_services_en'),
            'facebook_page' => $request->get('facebook_page'),
            'youtube_video' => $request->get('youtube_video'),
            'is_published' => $is_published
        ]);

        $location->save();

        $imagesData = [];
        $now = Carbon::now();

        for ($i = 0; $i < sizeof($images); $i++)
        {
            $i == 0 ? $is_thumbnail = true : $is_thumbnail = false;

            $imagesData[] = [
                'location_id' => $loc->id,
                'url' => $images[$i],
                'is_thumbnail' => $is_thumbnail,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Image::insert($imagesData);

        $languages = $request->get('languages');

        $languagesData = [];

        if (!is_null($languages)) {
            for ($i = 0; $i < sizeof($languages); $i++) {
                $languagesData[] = [
                    'language_id' => $languages[$i],
                    'location_id' => $loc->id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            LocationLanguage::insert($languagesData);
        }

        $months = $request->get('months');
        $locationMonths = [];

        if (!is_null($months)) {
            for ($i = 0; $i < sizeof($months); $i++) {
                $locationMonths[] = [
                    'month_id' => $months[$i],
                    'location_id' => $loc->id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            LocationMonth::insert($locationMonths);
        }

        return redirect()->route('admin.locations.edit.form', $loc->id)->with('info', 'Location has been successfully created');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload()
    {
        $file = Storage::disk('uploads')->put('images', request()->file('file'));

        return response()->json([
            'file' => $file
        ]);
    }

    /**
     * @return string JSON
     */
    public function json()
    {
        $locations = Location::with('category')->get();

        return json_encode($locations);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeThumbnail()
    {
        $location = request()->get('location');
        $image = request()->get('image');

        Image::where('location_id', $location)->update(['is_thumbnail' => false]);
        Image::where('id', $image)->update(['is_thumbnail' => true]);

        return response()->json(['status' => 'ok']);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function cropThumbnail()
    {
        $image = Storage::disk('uploads')->put('images', request()->file('image'));
        $id = request()->get('imageID');

        Image::where('id', $id)->update([
            'url' => 'uploads/' . $image
        ]);

        return response()->json(['status' => 'ok']);
    }

    /**
     * @param $id
     * @param LocationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, LocationRequest $request)
    {
        $images = $request->get('image');
        $is_published = ($request->get('is_published')) ? true : false;

        $location = Location::where('id', $id)->firstOrFail();

        $location->update([
            'category_id' => $request->get('category'),
            'sticker_id' => $request->get('sticker'),
            'email' => $request->get('email'),
            'mnumber' => $request->get('mnumber'),
            'working_from' => $request->get('working_from'),
            'working_to' => $request->get('working_to'),
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude'),
            'facebook_page' => $request->get('facebook_page'),
            'youtube_video' => $request->get('youtube_video'),
            'is_published' => $is_published
        ]);

        $location->translate('ka')->name = $request->get('name_ka');
        $location->translate('en')->name = $request->get('name_en');
        $location->translate('ka')->description = $request->get('description_ka');
        $location->translate('en')->description = $request->get('description_en');
        $location->translate('ka')->address = $request->get('address_ka');
        $location->translate('en')->address = $request->get('address_en');
        $location->translate('ka')->additional_services = $request->get('additional_services_ka');
        $location->translate('en')->additional_services = $request->get('additional_services_en');

        $location->save();

        $imagesData = [];
        $now = Carbon::now();

        for ($i = 0; $i < sizeof($images); $i++)
        {
            $i == 0 ? $is_thumbnail = true : $is_thumbnail = false;

            $imagesData[] = [
                'location_id' => $id,
                'url' => $images[$i],
                'is_thumbnail' => $is_thumbnail,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }

        Image::insert($imagesData);

        $languages = $request->get('languages');

        $languagesData = [];

        if (!is_null($languages)) {
            for ($i = 0; $i < sizeof($languages); $i++) {
                $languagesData[] = [
                    'language_id' => $languages[$i],
                    'location_id' => $id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }

            LocationLanguage::where('location_id', $id)->delete();

            LocationLanguage::insert($languagesData);
        }

        $months = $request->get('months');
        $locationMonths = [];

        if (!is_null($months)) {
            for ($i = 0; $i < sizeof($months); $i++) {
                $locationMonths[] = [
                    'month_id' => $months[$i],
                    'location_id' => $id,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            }
            LocationMonth::where('location_id', $id)->delete();
            LocationMonth::insert($locationMonths);
        }

        return redirect()->back()->with('info', 'Location has been successfully updated');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Location::where('id', $id)->delete();
        Image::where('location_id', $id)->delete();

        return redirect()->back();
    }
}
