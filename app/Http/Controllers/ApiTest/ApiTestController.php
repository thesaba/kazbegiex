<?php

namespace App\Http\Controllers\ApiTest;

use App\Http\Controllers\Controller;

/**
 * Class ApiTestController
 * @package App\Http\Controllers\ApiTest
 * @property integer method
 * @property array params
 * @property array|null extra
 */

class ApiTestController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('api.index');
    }

    /**
     * @param $id
     * @return array
     */
    public function sample($id)
    {
        /**
         * @var array $responseData
         */
        $responseData = [
            'method' => intval($id),
            'params' => array_except(config('api.method.'.$id.'.sample'), ['settings'])['data'],
            'extra'  => config('api.method.'.$id.'.sample.extra') ?? null
        ];

        if (request('messageId'))
            /**
             * @var array $responseData
             */
            $responseData['messageId'] = request('messageId');

        return $responseData;
    }
}
