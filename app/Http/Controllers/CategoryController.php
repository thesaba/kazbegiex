<?php

namespace App\Http\Controllers;

use App\Category;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        app()->setLocale(getPreviousLocale(url()->previous()));

        $categories = Category::all();

        return response()->json($categories);
    }
}
