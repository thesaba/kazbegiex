<?php

namespace App\Http\Controllers;

use App\Language;

class LanguageController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function json()
    {
        app()->setLocale(getPreviousLocale(url()->previous()));

        $languages = Language::all();

        return response()->json($languages);
    }
}
