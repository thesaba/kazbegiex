<?php

namespace App\Http\Controllers\Api\Services;

use App\Language;
use App\Location;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;

class LanguageController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLanguages()
    {
        $languages = Language::all();
        $formattedLanguages = [];

        foreach ($languages as $language)
        {
            $formattedLanguages[] = [
                'id' => $language->id,
                'code' => $language->code,
                'name' => getPropertyForModel($language, 'name')
            ];
        }

        return result($formattedLanguages);
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getLanguage()
    {
        $language = Language::find(param('id'));

        if (!$language)
            return error(IlluminateResponse::HTTP_NOT_FOUND);

        $formattedLanguage = getPropertyForModel($language, [ 'name' ], [
            'id' => $language->id,
            'code' => $language->code,
        ]);

        return result([$formattedLanguage]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocationLanguages()
    {
        $formattedLanguages = [];
        $locationId = param('locationId');

        $languages = Location::query()->find($locationId)->languages;

        foreach ($languages as $language)
        {
            $formattedLanguages[] = [
                'id' => $language->id,
                'code' => $language->code,
                'name' => getPropertyForModel($language, 'name')
            ];
        }

        return result($formattedLanguages);
    }
}
