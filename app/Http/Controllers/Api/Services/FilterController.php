<?php

namespace App\Http\Controllers\Api\Services;

use App\Location;
use App\Http\Controllers\Controller;

class FilterController extends Controller
{
    const PER_PAGE = 8;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFilteredLocations()
    {
        $pageId = !empty(param('pageId')) ? (int) param('pageId') : null;
        $categoryId = !empty(param('category')) ? (int) param('category') : null;
        $working_from = !empty(param('working_from')) ? (string) param('working_from') : null;
        $working_to = !empty(param('working_to')) ? (string) param('working_to') : null;
        $months = !empty(param('months')) ? (array) param('months') : null;
        $languageId = !empty(param('language')) ? (int) param('language') : null;
//        $categories = !empty(param('categories')) ? (array) param('categories') : null;

        $formattedLocations = [];

        $locations = Location::where('is_published', true)->with('thumbnail', 'sticker', 'months')->orderBy('created_at', 'desc');

        if ($categoryId != null && $categoryId != 0) {
            $locations->whereHas('category', function ($query) use ($categoryId) {
                $query->where('id', $categoryId);
            });
        }

//        if ($categories != null && !empty($categories)) {
//            if (is_array($categories)) {
//                for ($i = 0; $i < sizeof($categories); $i++) {
//                    $months[$i] = intval($months[$i]);
//                }
//            }
//
//            $locations->whereHas('category', function ($query) use ($categories) {
//                $query->whereIn('categories.id', $categories);
//            });
//        }

        if ($working_from != null && $working_from != '') {
            $locations->where('working_from', 'LIKE', $working_from);
        }

        if ($working_to != null && $working_to != '') {
            $locations->where('working_to', 'LIKE', $working_to);
        }

        if ($months != null && !empty($months)) {
            if (is_array($months)) {
                for ($i = 0; $i < sizeof($months); $i++) {
                    $months[$i] = intval($months[$i]);
                }
            }

            $locations->whereHas('months', function ($query) use ($months) {
                $query->whereIn('months.id', $months);
            });
        }

        if ($languageId != null && $languageId != 0) {
            $locations->whereHas('languages', function ($query) use ($languageId) {
                $query->where('languages.id', $languageId);
            });
        }

        $locationsCount = $locations->count();
        $paginator = [
            'count' => $locationsCount,
            'per_page' => self::PER_PAGE,
            'current_page' => $pageId,
            'last_page' => ceil($locationsCount / self::PER_PAGE),
            'next_page' => (($pageId + 1) <= ($locationsCount / self::PER_PAGE)) ? $pageId + 1 : null
        ];

        if ($pageId) {
            $locationsColl = $locations->get()->forPage($pageId, self::PER_PAGE);
        } else {
            $locationsColl = $locations->get();
        }

        foreach ($locationsColl as $location)
        {
            $formattedLocations[] = [
                'id' => $location->id,
                'category_id' => $location->category_id,
                'sticker_id' => $location->sticker_id,
                'name' => getPropertyForModel($location, 'name'),
                'email' => $location->email,
                'mnumber' => $location->mnumber,
                'working_from' => $location->working_from,
                'working_to' => $location->working_to,
                'latitude' => $location->latitude,
                'longitude' => $location->longitude,
                'address' => getPropertyForModel($location, 'address'),
                'facebook_page' => $location->facebook_page,
                'youtube_video' => $location->youtube_video,
                'description' => getPropertyForModel($location, 'description'),
                'additional_services' => getPropertyForModel($location, 'additional_services'),
                'is_published' => (bool) $location->is_published,
                'working_months' => $location->months,
                'images' => $location->images
            ];
        }

        if ($pageId) return result($formattedLocations, $paginator);

        return result($formattedLocations);
    }
}
