<?php

namespace App\Http\Controllers\Api\Services;

use App\Partner;
use App\Http\Controllers\Controller;
use App\Traits\Formattable;
use Illuminate\Http\Response as IlluminateResponse;

class PartnerController extends Controller
{
    use Formattable;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPartners()
    {
        return result($this->format(Partner::all()));
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getPartnerById()
    {
        $partnerId = param('partnerId');
        $partner = Partner::query()->find($partnerId);

        if (!$partner) return error(IlluminateResponse::HTTP_NOT_FOUND);

        return result($this->format($partner));
    }
}
