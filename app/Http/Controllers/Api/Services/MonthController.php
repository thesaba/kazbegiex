<?php

namespace App\Http\Controllers\Api\Services;

use App\Month;
use App\Http\Controllers\Controller;

class MonthController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMonths()
    {
        $months = Month::all();
        $formattedMonths = [];

        foreach ($months as $month)
        {
            $formattedMonths[] = [
                'id' => $month->id,
                'code' => $month->code,
                'name' => getPropertyForModel($month, 'name')
            ];
        }

        return result($formattedMonths);
    }
}
