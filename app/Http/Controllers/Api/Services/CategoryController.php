<?php

namespace App\Http\Controllers\Api\Services;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;

class CategoryController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        $categories = Category::all();
        $formattedCategories = [];

        foreach ($categories as $category)
        {
            $formattedCategories[] = [
                'id' => $category->id,
                'name' => getPropertyForModel($category, 'name'),
                'icon' => $category->icon
            ];
        }

        return result($formattedCategories);
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getCategory()
    {
        $category = Category::find(param('id'));

        if (!$category) {
            return error(IlluminateResponse::HTTP_NOT_FOUND);
        }

        $formattedCategory = getPropertyForModel($category, [ 'name' ], [
            'id' => $category->id,
            'icon' => $category->icon
        ]);

        return result([$formattedCategory]);
    }
}
