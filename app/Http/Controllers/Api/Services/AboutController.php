<?php

namespace App\Http\Controllers\Api\Services;

use App\About;
use App\Http\Controllers\Controller;
use App\Traits\Formattable;

class AboutController extends Controller
{
    use Formattable;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAbout()
    {
        return result($this->format(About::query()->first()));
    }
}
