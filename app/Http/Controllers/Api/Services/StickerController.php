<?php

namespace App\Http\Controllers\Api\Services;

use App\Sticker;
use App\Http\Controllers\Controller;

class StickerController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStickers()
    {
        $stickers = Sticker::all();

        return result($stickers);
    }
}
