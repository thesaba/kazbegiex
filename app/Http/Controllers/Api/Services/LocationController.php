<?php

namespace App\Http\Controllers\Api\Services;

use App\Location;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;

class LocationController extends Controller
{
    const PER_PAGE = 8;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocations()
    {
        $pageId = !empty(param('pageId')) ? (int) param('pageId') : null;
        $locations = Location::with(['images', 'sticker', 'category', 'months', 'languages']);
        $locationsCount = $locations->count();
        $paginator = [
            'count' => $locationsCount,
            'per_page' => self::PER_PAGE,
            'current_page' => $pageId,
            'last_page' => ceil($locationsCount / self::PER_PAGE),
            'next_page' => (($pageId + 1) <= ($locationsCount / self::PER_PAGE)) ? $pageId + 1 : null
        ];

        $formattedLocations = [];

        if ($pageId) {
            $locationsColl = $locations->get()->forPage($pageId, self::PER_PAGE);
        } else {
            $locationsColl = $locations->get();
        }

        foreach ($locationsColl as $location)
        {
            $formattedLanguages = [];

            foreach ($location->languages as $language) {
                $formattedLanguages[] = [
                    'id' => $language->id,
                    'code' => $language->code,
                    'name' => getPropertyForModel($language, 'name')
                ];
            }

            $formattedLocations[] = [
                'id' => $location->id,
                'category_id' => $location->category_id,
                'category' => getPropertyForModel($location->category, 'name'),
                'category_icon' => $location->category->icon,
                'sticker_id' => $location->sticker_id,
                'name' => getPropertyForModel($location, 'name'),
                'email' => $location->email,
                'mnumber' => $location->mnumber,
                'working_from' => $location->working_from,
                'working_to' => $location->working_to,
                'latitude' => $location->latitude,
                'longitude' => $location->longitude,
                'address' => getPropertyForModel($location, 'address'),
                'facebook_page' => $location->facebook_page,
                'youtube_video' => $location->youtube_video,
                'description' => getPropertyForModel($location, 'description'),
                'additional_services' => getPropertyForModel($location, 'additional_services'),
                'is_published' => (bool) $location->is_published,
                'working_months' => $location->months,
                'images' => $location->images,
                'notification' => getPropertyForModel($location, 'notification'),
                'languages' => $formattedLanguages
            ];
        }

        if ($pageId) return result($formattedLocations, $paginator);

        return result($formattedLocations);
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function getLocation()
    {
        $locationQuery = Location::with(['images', 'sticker', 'category', 'languages']);
        $location = $locationQuery->find(param('id'));
        $locationLanguages = $location->languages;
        $formattedLanguages = [];

        if (!$location) {
            return error(IlluminateResponse::HTTP_NOT_FOUND);
        }

        foreach ($locationLanguages as $language)
        {
            $formattedLanguages[] = [
                'id' => $language->id,
                'code' => $language->code,
                'name' => getPropertyForModel($language, 'name')
            ];
        }

        $formattedLocation = getPropertyForModel($location, ['name', 'description', 'address', 'additional_services'], [
            'id' => $location->id,
            'category_id' => $location->category_id,
            'category' => getPropertyForModel($location->category, 'name'),
            'category_icon' => $location->category->icon,
            'sticker_id' => $location->sticker_id,
            'email' => $location->email,
            'mnumber' => $location->mnumber,
            'working_from' => $location->working_from,
            'working_to' => $location->working_to,
            'latitude' => $location->latitude,
            'longitude' => $location->longitude,
            'facebook_page' => $location->facebook_page,
            'youtube_video' => $location->youtube_video,
            'is_published' => (bool) $location->is_published,
            'working_months' => $location->months,
            'images' => $location->images,
            'notification' => getPropertyForModel($location, 'notification'),
            'languages' => $formattedLanguages
        ]);

        return result([$formattedLocation]);
    }
}
