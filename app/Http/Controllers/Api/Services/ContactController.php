<?php

namespace App\Http\Controllers\Api\Services;

use App\Contact;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getContactInformation()
    {
        $contactInformation = Contact::first(['address', 'email', 'mnumber']);

        return result([$contactInformation]);
    }
}
