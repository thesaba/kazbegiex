<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

/**
 * Class ApiController
 * @package App\Http\Controllers\Api
 */
class ApiController extends Controller
{
    /**
     * @return array
     */
    public function index()
    {
        if (!request()->get('method'))
            return error(1);

        if (!isset(config('api.method')[request()->get('method')]))
            return error(404);

        $method = config('api.method')[request()->get('method')];

        return app('App\Http\Controllers\Api\Services\\' . $method['methodController'])->{camel_case($method['methodName'])}();
    }
}
