<?php

namespace App\Http\Controllers;

use Illuminate\Filesystem\Filesystem;

class LangController extends Controller
{
    public function init()
    {
        $langs = [];

        $files = (new Filesystem())->files(resource_path('lang/' . app()->getLocale()));

        foreach ($files as $file) {
            $name = trim($file->getRelativePathname(), '.php');
            $langs[$name] = trans($name);
        }

        header('Content-Type: text/javascript');
        echo('window.i18n = ' . json_encode($langs) . ';');

        exit();
    }
}
