<?php

namespace App\Http\Controllers;

use App\Mail\ResetPassword;
use App\PasswordReset;
use App\User;
use Illuminate\Support\Facades\Mail;

/**
 * Class PasswordresetController
 * @package App\Http\Controllers
 */
class PasswordresetController extends Controller
{
    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function init()
    {
        /**
         * @var integer $users_count
         * @var string $token
         */

        list($email) = array_values(request()->validate([
            'email' => 'required|email'
        ]));

        $users_count = optional(User::where('email', '=', $email))->count();

        if ($users_count == 0)
            return redirect()->back()->with('info', 'User not found');

        $token = md5($email);

        PasswordReset::create([
            'email' => $email,
            'token' => $token
        ]);

        Mail::to($email)
            ->queue(new ResetPassword($token));

        return redirect()->back()->with('info', 'An email with a reset link has been sent. Please check your inbox');
    }

    /**
     * @param $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm($token)
    {
        /**
         * @var array $reset_data
         * @var string $email
         */
        $reset_data = PasswordReset::where('token', '=', $token)->firstOrFail();
        $email = $reset_data->email;

        return view('admin.auth.reset_password', compact('email', 'token'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset()
    {
        /**
         * @var string $password
         * @var string $email
         */

        list($password, $token) = array_values(request()->validate([
            'password' => 'required|min:3',
            'reset_token' => 'required'
        ]));

        $password = bcrypt($password);

        $email = PasswordReset::where('token', '=', $token)->firstOrFail()->email;

        User::where('email', '=', $email)->update([
            'password' => $password
        ]);

        return redirect()->route('admin.index')->with('info', 'Password has been successfully changed');
    }
}
