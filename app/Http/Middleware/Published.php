<?php

namespace App\Http\Middleware;

use App\Location;
use Closure;

class Published
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $location = Location::where('id', $request->route()->id)->firstOrFail();

        if (! $location->is_published) return redirect()->route('map');

        return $next($request);
    }
}
