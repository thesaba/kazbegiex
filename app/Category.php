<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App
 * @property string name
 */

class Category extends Model
{
    use Translatable;

    protected $table = 'categories';

    protected $fillable = ['icon'];

    public $timestamps = false;

    public $translatedAttributes = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany(Location::class);
    }
}
