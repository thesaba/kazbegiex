<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Contact
 * @package App
 * @property string address
 * @property string email
 * @property string mnumber
 */

class Contact extends Model
{
    protected $table = 'contact';

    protected $fillable = ['address', 'email', 'mnumber'];
}
