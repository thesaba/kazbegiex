<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Location
 * @package App
 * @property integer id
 * @property integer category_id
 * @property integer sticker_id
 * @property string email
 * @property string mnumber
 * @property string latitude
 * @property string longitude
 * @property string facebook_page
 * @property string youtube_video
 * @property string name
 * @property mixed working_from
 * @property mixed working_to
 * @property string description
 * @property string additional_services
 * @property string notification
 * @property string address
 * @property boolean is_published
 */

class Location extends Model
{
    use Translatable;

    protected $table = 'locations';

    protected $fillable = [
        'category_id',
        'sticker_id',
        'email',
        'mnumber',
        'working_from',
        'working_to',
        'latitude',
        'longitude',
        'facebook_page',
        'youtube_video',
        'is_published'
    ];

    public $translatedAttributes = [
        'name',
        'description',
        'additional_services',
        'address',
        'notification'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function thumbnail()
    {
        return $this->hasOne(Image::class)->where('is_thumbnail', true);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'location_languages');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function months()
    {
        return $this->belongsToMany(Month::class, 'location_months');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sticker()
    {
        return $this->belongsTo(Sticker::class);
    }

    /**
     * @return string
     */
    public function getWorkingHoursAttribute()
    {
        return "{$this->working_from} - {$this->working_to}";
    }
}
