<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;

trait Formattable {
    /**
     * @param $model
     * @return array|\Illuminate\Support\Collection
     */
    private function format($model)
    {
        if ($model instanceof Collection) {
            return $model->map(function($item) {
                return $this->fetch($item);
            });
        }

        return $this->fetch($model);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $item
     * @return \Illuminate\Support\Collection
     */
    private function fetch($item)
    {
        $translatedAttributes = get_object_vars($item)['translatedAttributes'] ?? [];
        $keys = $item->getFillable();
        $regularKeys = array_diff($keys, $translatedAttributes);

        return $this->combine($regularKeys, $translatedAttributes, $item);
    }

    /**
     * @param array $regularAttributes
     * @param array $translatedAttributes
     * @param $item
     * @return \Illuminate\Support\Collection
     */
    private function combine(array $regularAttributes, array $translatedAttributes, $item)
    {
        $formattedItem = collect();
        $formattedItem->put('id', $item['id']);

        foreach ($regularAttributes as $attribute) {
            $formattedItem->put($attribute, $item[$attribute]);
        }

        foreach ($translatedAttributes as $attribute) {
            $formattedItem->put($attribute, getPropertyForModel($item, $attribute));
        }

        return $formattedItem;
    }
}