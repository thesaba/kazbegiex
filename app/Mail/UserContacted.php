<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserContacted extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;

    protected $message;

    /**
     * Create a new message instance.
     *
     * @param $email
     * @param $message
     */
    public function __construct($email, $message)
    {
        $this->email = $email;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from($this->email)
            ->with([
                'email' => $this->email,
                'message' => $this->message
            ])
            ->markdown('emails.user.message');
    }
}
