<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LocationLanguage
 * @package App
 * @property integer location_id
 * @property integer language_id
 */

class LocationLanguage extends Model
{
    protected $table = 'location_languages';

    protected $fillable = ['language_id', 'location_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function languages()
    {
        return $this->hasMany(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locations()
    {
        return $this->hasMany(Location::class);
    }
}
