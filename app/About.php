<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class About
 * @package App
 * @property string about
 * @property string image
 */

class About extends Model
{
    use Translatable;

    protected $table = 'about';

    protected $fillable = ['image'];

    public $translatedAttributes = ['about'];
}
