<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthTranslation extends Model
{
    protected $table = 'month_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
