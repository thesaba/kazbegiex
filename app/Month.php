<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    use Translatable;

    protected $table = 'months';

    protected $fillable = ['code'];

    public $translatedAttributes = ['name'];
}
