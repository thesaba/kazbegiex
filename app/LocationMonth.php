<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationMonth extends Model
{
    protected $table = 'location_months';

    protected $fillable = ['location_id', 'month_id'];

    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    public function months()
    {
        return $this->hasMany(Month::class);
    }
}
