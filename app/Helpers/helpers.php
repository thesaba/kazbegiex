<?php

if (!function_exists('param')) {
    /**
     * @param null $paramName
     * @return array|\Illuminate\Http\Request|mixed|string
     */
    function param($paramName = null)
    {
        if (!is_null($paramName))
            return request('params')[camel_case($paramName)] ?? '';

        return request('params');
    }
}

if (!function_exists('params')) {
    /**
     * @param array ...$params
     * @return array
     */
    function params(...$params)
    {
        $paramsValues = [];

        foreach ($params as $param) {
            $paramsValues[snake_case($param)] = param($param);
        }

        return $paramsValues;
    }
}

if (!function_exists('resultMethodId')) {
    /**
     * @return int
     */
    function resultMethodId()
    {
        return ((int)request()->get('method')) + 1;
    }
}

if (!function_exists('resultMethodType')) {
    /**
     * @return mixed
     */
    function resultMethodType()
    {
        return config('api.method')[resultMethodId()]['messageType'];
    }
}

if (!function_exists('result')) {
    /**
     * @param $params
     * @param array|null $paginator
     * @return \Illuminate\Http\JsonResponse
     * @internal param bool $paginated
     */
    function result($params, $paginator = null)
    {
        $messageId = !empty(param('messageId')) ? (int) param('messageId') : null;
        $responseJson = ['method' => resultMethodId(), 'params' => $params, 'messageId' => $messageId, 'type' => resultMethodType(), 'errorCode' => 0];

        if ($paginator) {
            $paginatorData = [
                'paginator' => $paginator
            ];

            $responseJson = array_merge($responseJson, $paginatorData);
        }

        return response()->json($responseJson);
    }
}

if (!function_exists('getPropertyForModel')) {
    /**
     * @param \Dimsav\Translatable\Translatable $model
     * @param $properties
     * @param array $data
     * @return array|mixed
     */
    function getPropertyForModel($model, $properties, $data = []) {
        $properties = ($wasArray = is_array($properties)) ? $properties : [$properties];

        foreach($properties as $property) {
            foreach(config('translatable.locales') as $index => $locale) {
                $data[$property][] = [
                    'languageCode' => $index + 1,
                    'data' => removeTags($model->translate($locale)->{$property})
                ];
            }
        }

        return $wasArray ? $data : $data[$properties[0]];
    }
}

if (!function_exists('error')) {
    /**
     * @param $code
     * @return array
     */
    function error($code)
    {
        $errorDescription = config('api.error-code')[$code];
        $messageId = !empty(param('messageId')) ? (int) param('messageId') : null;

        return [
            'errorCode' => $code,
            'errorDescription' => $errorDescription,
            'messageId' => $messageId
        ];
    }
}

if (!function_exists('stripLocationTags')) {
    /**
     * @param $locations
     * @return mixed
     */
    function stripLocationTags($locations)
    {
        foreach ($locations as $location) {
            $description = $location->description;
            $additional_services = $location->additional_services;

            $location->description = strip_tags($description);
            $location->additional_services = strip_tags($additional_services);
        }

        return $locations;
    }
}

if (!function_exists('removeTags')) {
    /**
     * @param $attribute
     * @return string
     */
    function removeTags($attribute)
    {
        return strip_tags($attribute);
    }
}

if (!function_exists('junkCount')) {
    /**
     * @return int
     */
    function junkCount()
    {
        $localImagesCount = 0;

        for ($i = 1; $i <= sizeof(glob('uploads/images' . '/*'));  $i++)
        {
            $localImagesCount++;
        }

        $imagesCount = \App\Image::all()->count();

        $junkCount = $localImagesCount - $imagesCount;

        return $junkCount;
    }
}

if (!function_exists('getPreviousLocale')) {
    /**
     * @param $url
     * @return mixed
     */
    function getPreviousLocale($url)
    {
        $parsed = parse_url($url);
        $path = $parsed['path'];
        $path_parts = explode('/', $path);

        return $path_parts[1];
    }
}

if (!function_exists('_dd')) {
    /**
     * @param array ...$var
     */
    function _dd(...$var) {
        die(json_encode($var, JSON_PRETTY_PRINT));
    }
}