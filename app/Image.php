<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App
 * @property integer location_id
 * @property string url
 * @property boolean is_thumbnail
 */

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'location_id',
        'url',
        'is_thumbnail'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * @param $value
     * @return string
     */
    public function getUrlAttribute($value)
    {
        return asset($value);
    }

    public function getIsThumbnailAttribute($value)
    {
        return (bool)$value;
    }
}
