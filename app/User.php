<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App
 * @property string fname
 * @property string lname
 * @property string mnumber
 * @property string email
 * @property string username
 * @property string password
 * @property boolean isAdmin
 */

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'fname', 'lname', 'mnumber', 'email', 'username', 'password', 'isAdmin'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return (auth()->user()->isAdmin == 1) ? true : false;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return auth()->user()->fname . ' ' . auth()->user()->lname;
    }
}
