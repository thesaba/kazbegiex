<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTranslation extends Model
{
    protected $table = 'location_translations';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'additional_services',
        'address',
        'notification'
    ];
}
