<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Partner
 * @package App
 * @property string image
 * @property string title
 * @property string description
 */

class Partner extends Model
{
    use Translatable;

    protected $table = 'partners';

    protected $fillable = ['image'];

    public $translatedAttributes = ['title', 'description'];
}
