<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LanguageTranslation extends Model
{
    protected $table = 'language_translations';

    public $timestamps = false;

    protected $fillable = ['name'];
}
