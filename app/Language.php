<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 * @package App
 * @property string code
 * @property string name
 */

class Language extends Model
{
    use Translatable;

    protected $table = 'languages';

    protected $fillable = ['code'];

    public $translatedAttributes = ['name'];
}
