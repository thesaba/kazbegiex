<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sticker
 * @package App
 * @property string name
 * @property string color
 * @property string content
 */

class Sticker extends Model
{
    protected $table = 'stickers';

    protected $fillable = [
        'name',
        'color',
        'content'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
