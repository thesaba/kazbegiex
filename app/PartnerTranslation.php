<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerTranslation extends Model
{
    protected $table = 'partner_translations';

    public $timestamps = false;

    protected $fillable = ['title', 'description'];
}
